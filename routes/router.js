var express = require('express');
var router = express.Router();
var ws = require("../app");
var WebSocket = require("ws");
var User = require('../models/user');
var url = require('url');
var iplocation = require('iplocation')
var stream_config = require("../stream_config.json")
var is_running = require('is-running')
const spawn = require('child_process').spawn;

async function getUserInfo (req) {
	data = {
		loggedIn: false,
		isAdmin: false,
		username : undefined,
	}
	if (!!req.session.userId) {
		var user = await User.findOne({_id: req.session.userId})
		if (user) {
			data.loggedIn = true;
			data.username = user.username;
		} 
		if (user.isAdmin) {
			data.isAdmin = true
		}
		return data;
	} else {
		return data;
	}
}

var genRandomString = () => {
	return Math.random().toString(36).substring(4);
}

var random_ips=[
'177.120.50.219',
'213.198.161.206',
'44.96.62.249',
'23.15.237.123',
'91.159.54.29'
]

var firstPacket = []
var users = []
var userInfo = []
var wsAdminClients = []

router.get("/", async (req, res, next) =>{
	console.log(userInfo);
	console.log(req.session.username);
	res.render('index', {sessionInfo: await getUserInfo(req)})
})

router.get("/dashboard", isAdmin, async (req, res, next) => {
	res.render('dash', {sessionInfo: await getUserInfo(req)})
})

var broadcast = (clients, data, key) =>{
	clients.forEach((client) => {
		if (client[key].readyState === WebSocket.OPEN) {
			client[key].send(data)
		}
	})
}

var removeItem = (object, key, value) => {
		if (value == undefined)
				return;

		for (var i in object) {
				if (object[i][key] == value) {
						object.splice(i, 1);
				}
		}
};


router.ws("/dashboard/getInfo", async (ws, req) =>{
	console.log(req.session.userId);
	console.log(req.session.isAdmin);
	console.log(!req.session.isAdmin )
	if (!req.session.isAdmin )  {
		ws.reject();
	}

	wsAdminId = genRandomString()
	wsAdminClients.push({'ws' : ws, "wsId":wsAdminId});
	req.session.adminId = wsAdminId

	ws.on('close', () => {
		removeItem(wsAdminClients, "wsId", req.session.adminId);
	})

	ws.on('message', (m) => {
		if (m == "getUserInfo") {
			data = {clients : userInfo,
							event_type: "create_all"}

			ws.send(JSON.stringify(data))
		}
	})
})

router.get("/manage_streams", async (req, res, next) => {

  if (!is_running(req.session.hq_stream_pid)) {
    req.session.hq_stream_pid = undefined;
  }
  console.log(req.session.hq_stream_pid);
  res.render('manage_streams', {sessionInfo: await getUserInfo(req),
                                stream_config: stream_config,
                                pid: req.session.hq_stream_pid});
})


router.get("/manage_streams/start", async (req, res, next) => {
  proc = spawn('ffmpeg', [stream_config.hq_stream], 
    {shell: true, stdio:'ignore'}
  );
  req.session.hq_stream_pid = proc.pid;

  res.redirect("/manage_streams");

})

router.get("/manage_streams/stop", async (req, res, next) => {
  console.log(req.session.hq_stream_pid)
  process.kill(req.session.hq_stream_pid);
  req.session.hq_stream_pid = undefined;  
  res.redirect("/manage_streams");

})







router.get("/stream", isLoggedIn, async (req, res, next) =>{
	res.render('view', {sessionInfo: await getUserInfo(req)})
})

router.ws('/ws_stream', async function(ws, req) {
	if (!!req.userId) {
		ws.reject();
	}
	

	var wsId = genRandomString();
	// ip =random_ips[ Math.floor(Math.random()*random_ips.length) ]
	ip = req.clientIp
	ipLoc = await iplocation(ip)

	currentUserInfo = {
		'user_agent':req.headers['user-agent'],
		'ip' : ip,
		'userId': req.session.userId,
		'username' : req.session.username,
		'wsId' : wsId,
		'event_type' : 'create',
		'long_lat' : [ipLoc.latitude,  ipLoc.longitude],
	}
	

	req.session.wsId = wsId;
	userInfo.push(currentUserInfo);
	users.push({"ws" : ws, "wsId" : wsId});

	broadcast(wsAdminClients, JSON.stringify(currentUserInfo), 'ws');

	ws.on('close', () => {
		removeItem(userInfo, "wsId", req.session.wsId);
		removeItem(users, 'wsId', req.session.wsId);
		data = {"wsId": req.session.wsId, "event_type": "delete"}
		broadcast(wsAdminClients, 
							JSON.stringify(data), 
							'ws');
	})
});


router.post("/ws/:secret", (request, response, next) => {
	var params = request.params['secret']

	if (params !== "bla") {
		console.log(
			'Failed Stream Connection: '+ request.socket.remoteAddress + ':' +
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	}

	response.connection.setTimeout(0);
	console.log(
		'Stream Connected: ' + 
		request.socket.remoteAddress + ':' +
		request.socket.remotePort
	);
	request.on('data', function(data){

		users.forEach((client) => {
			if (client['ws'].readyState === WebSocket.OPEN) {
				client['ws'].send(data)
			}
		})

	});
	request.on('end',function(){
	});
})

/*
LOGIN
*/
router.get("/login", async (req, res, next) => {
	res.render('login', {sessionInfo: await getUserInfo(req)})
})

router.post('/login', (req, res, next) => {
	User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
		if (error || !user) {
			res.redirect("/login?status=fail")
		} else {
			req.session.userId = user._id;
			req.session.username = user.username;
			req.session.isAdmin = user.isAdmin;
			return res.redirect('/stream?status=success');
		}
	});
})


/*
SIGNUP
*/
router.get('/signup', (req, res, next) => {
	res.render('signup')
})

router.post("/signup", (req, res, next) => {
	var userData = {
		username: req.body.username,
		password: req.body.password,
		passwordConf: req.body.passwordConf,
	}

	User.create(userData, function (error, user) {
		if (error) {
			console.log(error);
			return next(error);
		} else {
			req.session.userId = user._id;
			return res.redirect('/stream');
		}
	});

})

function isLoggedIn(req, res, next){
	if (req.session.userId) {
		return next()
	}

	res.redirect("login")
}

function isAdmin(req, res, next){
	if (req.session.userId) {
		User.findOne({_id: req.session.userId}, (err, user) => {
			if (user && user.isAdmin) {
				next();
			} else {
				res.redirect("login")
				res.end();
			}

		})
	} else {
		res.redirect("login")
		res.end()
	}
}

// GET route after registering
router.get('/user_list', isAdmin, async (req, res, next) => {
	session_info = await getUserInfo(req)
	User.find({}, (err, users) => {
		res.render("user_list", {users:users, sessionInfo: session_info})
	})
});

router.get('/user_list/add', isAdmin, async function (req, res, next) {
	session_info = await getUserInfo(req);
	User.find({}, (err, users) => {
		res.render("add_user", {users:users, sessionInfo: session_info})
	})
});

router.post('/user_list/add', isAdmin, function (req, res, next) {
	var userData = {
		username: req.body.username,
		password: req.body.password,
		passwordConf: req.body.passwordConf,
	}

	User.create(userData, function (error, user) {
		if (error) {
			return next(error);
		} else {
			return res.redirect('/user_list?status=success');
		}
	});
});

router.get("/user_list/:username", isAdmin, async (req, res, next) => {
	var session_info  = await getUserInfo(req)
	User.find({username: req.params['username']}, (err, user) => {
		res.render('user_profile', {user:user[0], sessionInfo: session_info});
	})
})


router.post('/user_list/:username/change_pass', isAdmin, (req, res,next) => {
	if (req.body['pass'] != req.body['pass_confirm']) {
		res.redirect('/user_list/' + req.params['username'] + "?status=fail")
		return 
	}
	User.findOne({username: req.params['username']}, (err, user) => {
		user.password = req.body['pass']
		user.save((err) => {
				res.redirect('/user_list/' + req.params['username'] + "?status=success")
		})
	})
})

router.get('/user_list/:username/delete', isAdmin, (req, res,next) => {
	User.findOneAndRemove({username: req.params['username']}, (err) => {
		if (err) {
			console.log(err)
		} else {
			res.redirect("/user_list?status=user_deleted")
		}

	})
})

// GET for logout logout
router.get('/logout', function (req, res, next) {
	if (req.session) {
		// delete session object
		req.session.destroy(function (err) {
			if (err) {
				return next(err);
			} else {
				return res.redirect('/login');
			}
		});
	}
});

module.exports = router;