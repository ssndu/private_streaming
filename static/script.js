(function($) {

	status = $(location).attr('search').split("=")[1]

	if (status == 'fail') {
		$(".alert-danger").toggleClass("show");
	}

	if (status == 'success') {
		$(".alert-success").toggleClass("show");
	}
	if (status == 'user_deleted') {
		$(".user_deleted").toggleClass("show");
	}

	url = $(location).attr('href').split("/").slice(-1)[0]

	if (url.includes("stream")) {

		host = document.location.host;


		// $(".jsmpeg").attr('data-url', "wss://" + document.location.host + "/ws_stream");
		var player = new JSMpeg.Player("wss://" + document.location.host + "/ws_stream", 
										 {canvas: document.getElementById("jsmpeg"), loop: true, autoplay: true});

		$('.player-pause').click(function() {
			$(this).toggleClass('hidden');
			$('.player-play').toggleClass("hidden");
			player.pause();
		})
		$('.player-play').click(function() {
			$(this).toggleClass('hidden');
			$('.player-pause').toggleClass("hidden");
			player.play();
		})


		 var pfx = ["webkit", "moz", "ms", "o", ""];
		 function RunPrefixMethod(obj, method) {
		 	
		 	var p = 0, m, t;
		 	while (p < pfx.length && !obj[m]) {
		 		m = method;
		 		if (pfx[p] == "") {
		 			m = m.substr(0,1).toLowerCase() + m.substr(1);
		 		}
		 		m = pfx[p] + m;
		 		t = typeof obj[m];
		 		if (t != "undefined") {
		 			pfx = [pfx[p]];
		 			return (t == "function" ? obj[m]() : obj[m]);
		 		}
		 		p++;
		 	}

		 }

		 var e = document.getElementById("fullscreen");
		var el = document.getElementById("jsmpeg")
		 e.onclick = function() {
		
		     if (RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen")) {
		         RunPrefixMethod(document, "CancelFullScreen");
		     }
		     else {
		         RunPrefixMethod(el, "RequestFullScreen");
		     }
		
		}
	}
	if (url.includes("dashboard")) {
		function AppViewModel() {
			var self = this;

			self.viewers = ko.observableArray([]);

			self.addViewer = function(client) {
				console.log(client);
				self.viewers.push(client);
			};

			self.removePerson = function(wsId) {
				self.viewers.remove(self.viewers().find(x=>x.wsId == wsId));
			}
		}

		model  = new AppViewModel();
		
		ko.applyBindings(model);

		markers = []
		users = []

		var map = tomtom.map('map', {
			key: 'RiJ9WARlSgoSfSNBhDq6hisiBMSNSSHv',
			source: 'vector',
			basePath: document.location.origin + '/tomtom'
		})

		map.setView([47.0990929,28.8215368], 0);


		ws = new WebSocket("wss://" + document.location.host + "/dashboard/getInfo");
		ws.onopen = () => {
			ws.send('getUserInfo');
		}
		
		ws.onmessage = (m) => {

			data = JSON.parse(m.data);
			if (data.event_type == "delete") {
				ws = data.wsId
				markers.find(x=>x.id == ws).marker.remove()
				model.removePerson(ws)
			}

			if (data.event_type == "create") {
				users.push(data)
				model.addViewer(data);
				marker = tomtom.L.marker(
					new tomtom.L.LatLng(data.long_lat[0], 
										data.long_lat[1]), 
										{title: data.username});
				marker.bindPopup(data.username)
				marker.addTo(map);
				markers.push({marker: marker, id : data.wsId})
				window.m = marker
				
			}

			if (data.event_type == "create_all") {
				users = data.clients
				if (users.length != 0 ) {
					users.forEach((client) => {
						model.addViewer(client);
						marker = tomtom.L.marker(
							new tomtom.L.LatLng(client.long_lat[0], 
												client.long_lat[1]), 
												{title: client.username});
						marker.bindPopup(client.username)
						marker.addTo(map);
						markers.push({marker: marker, id : client.wsId})
					})
				}
			}
		}
	}


})(jQuery);