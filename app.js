var express = require('express');
var app = module.exports = express(); 
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var fs = require("fs");
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var https = require('https')
require('dotenv').config()
const requestIp = require('request-ip');

var MONGODB_URI = 'mongodb://';

if (process.env.USERNAME) {
  MONGODB_URI += process.env.USERNAME;
}
if (process.env.PASS) {
  MONGODB_URI += ":" + process.env.PASS + "@";
}

MONGODB_URI += process.env.HOST

if (process.env.DB_PORT) {
  MONGODB_URI += ":" + process.env.DB_PORT;
}

if (process.env.DB) {
  MONGODB_URI += "/" + process.env.DB;
}



app.use(requestIp.mw())

var certOptions = {
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.crt')
}
var expressWs = require('express-ws')



var server = https.createServer(certOptions, app)

ws = expressWs(app, server)

//connect to MongoDB
mongoose.connect(MONGODB_URI,{useMongoClient: true });
var db = mongoose.connection;
app.set('view engine','ejs');
//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
});

//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  cookie: {secure:false, httpOnly: false},
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// serve static files from template
app.use(express.static(__dirname + '/static'));

// include routes
var routes = require('./routes/router');
app.use('/', routes);



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});

// const { exec } = require('child_process');
// exec('ffmpeg -f avfoundation -framerate 30 -video_size 1280x720 -i "0" -f mpegts -codec:v mpeg1video -s 640x480 -b:v 1000k -bf 0 https://localhost:4443/ws/bla/', (err, stdout, stderr) => {
//   if (err) {
//     // node couldn't execute the command
//     return;
//   }

//   // the *entire* stdout and stderr (buffered)
//   // console.log(`stdout: ${stdout}`);
//   console.log(`stderr: ${stderr}`);
// });


server.listen(4443)

