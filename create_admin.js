var mongoose = require('mongoose');
require('dotenv').config()


var MONGODB_URI = 'mongodb://';

if (process.env.USERNAME) {
  MONGODB_URI += process.env.USERNAME;
}
if (process.env.PASS) {
  MONGODB_URI += ":" + process.env.PASS + "@";
}
MONGODB_URI += process.env.HOST
if (process.env.DB_PORT) {
  MONGODB_URI += ":" + process.env.DB_PORT;
}
if (process.env.DB) {
  MONGODB_URI += "/" + process.env.DB;
}



mongoose.connect(MONGODB_URI, {useMongoClient: true});
// var db = mongoose.connection;
var User = require('./models/user');
let prompt = require('password-prompt')


var readline = require('readline-sync');

var userData = {
  username: "",
  password: "",
  passwordConf: "",
  isAdmin: true,
}
var username = readline.question("New admin's username: ");
var pass;
userData.username = username;
let password = prompt('Enter the password: ')

var createAdmin = (data) => {
	User.create(data, function (error, user) {
	  if (error) {
	  	console.log("Ann error occured")
	    console.log(error);
	    process.exit()
	  }  else {
	  	console.log("New admin created successfully")
	  }

	  process.exit()
	});
}

password.then((p) => {
	pass = p;
	prompt("confirm the password: ").then((p1) => {
		if (p1==pass) {
			userData.password = userData.passwordConf = pass;
			createAdmin(userData);
		} else {
			console.log("Passwords don't match. Please try again.")
			process.exit();
		}
	})
})


